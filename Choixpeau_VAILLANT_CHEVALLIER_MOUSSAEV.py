# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from math import sqrt
import csv

caracteristique = ['Courage', 'Ambition', 'Intelligence', 'Good']


with open("Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    csv1 = [{key : value for key, value in element.items()} for element in reader]


print(csv1)
with open("Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    csv2 = [{key : value for key, value in element.items()} for element in reader]
print(csv2)
liste_persos = []
for element in csv2:
    for element2 in csv1:
        if element['Name'] == element2['Name']:
            liste_persos.append(element)
            liste_persos[-1].update(element2)
            del liste_persos[-1]["Id"]

print(liste_persos)

profils_inconnus = [{'Courage': 9, 'Ambition': 2, 'Intelligence': 8, 'Good': 9},
                  {'Courage': 6, 'Ambition': 7, 'Intelligence': 9, 'Good': 7},
                  {'Courage': 3, 'Ambition': 8, 'Intelligence': 6, 'Good': 3},
                  {'Courage': 2, 'Ambition': 3, 'Intelligence': 7, 'Good': 8},
                  {'Courage': 3, 'Ambition': 4, 'Intelligence': 8, 'Good': 8}]
                  


def distance(profil_inconnu, profil_connu):
    '''
    calculer la distance entre le profil inconnu et le profil connu
    profil_inconnu = tuple d'entiers caractérisant le profil
    profil_connu = tuple d'entiers caractérisant le profil
    La sortie sera un flottant caractérisant la distance entre les deux élèves
    '''

    return sqrt((int(profil_inconnu['Courage']) - int(profil_connu['Courage']))**2
                + (int(profil_inconnu['Ambition']) - int(profil_connu['Ambition']))**2
                + (int(profil_inconnu['Intelligence']) - int(profil_connu['Intelligence']))**2
                + (int(profil_inconnu['Good']) - int(profil_connu['Good']))**2)


def kppv(profil_inconnu, liste_poudlard, k=5):
    '''
    Fonction qui permet de trouver la maison d'un profil donné en utilisant
    l'algorithme des k plus proches voisins
    Paramètres :
        Profil_inconnu : personnage à ranger dans une maison
        Profil_connu : liste des élèves de Poudlard que l'on compare au profil_inconnu
        k = 5 : est le nombre de voisins que l'on va considérer
    Sortie : Renvoie la maison trouvée pour le profil_inconnu
    '''

    for eleve in liste_poudlard:

        eleve['Distance'] = distance(eleve, profil_inconnu)
           
    liste_poudlard.sort(key=lambda dico: dico['Distance'])
    print(liste_poudlard)
    
    return liste_poudlard[:k]

    
    

def ihm():
    '''
    Fonction lançant l'IHM.
    '''
    print("Voici la maison à laquelle appartiennent les personnages")
    for element in profil_inconnu:
        print(kppv(profil_inconnu, caracteristique, profil_connu))
        print("Les voisins de ce profil sont :")
        for i in range(k):
            print(liste_distance[i].key)

#Lancement du programme:
 
print(kppv(profils_inconnus[0], liste_persos))
    
