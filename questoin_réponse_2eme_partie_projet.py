def questionnaire(question):
    question_1 = "Comment réagissez-vous si on essaie de vous racketter ?" #Courage
    réponse = "Je cours.", "J'essaie de discuter avec les ravisseurs.", "Je me défends."
    question_2 = "En dehors des cours de magie tu préfères ?" #Intelligence
    réponse = "Prendre un livre et m'instruire.", "Sortir avec des amis", "Aller embêter les autres élèves.", "réviser mes cours"
    question_3 = "Ton ami a besoin d'argent..." #Bonté
    réponse = "Je lui donne sans attendre de remboursement", "Je lui donne en imposant une taxe pour le remboursement", "J'hésite à lui donner et ne craque pas."
    question_4 = "Vous entendez des bruits bizarres la nuit, que faites vous ?" #Courage
    réponse = "Je sors de mon lit et je vais voir ce qu'il se passe.", "Je me cache sous ma couette.", "J'utilise ma cape d'invisibilité pour aller voir se qu'il se passe."
    question_5 = "Pour votre projet personnel seriez-vous prêt à ?" #Ambition
    réponse = "Vous éloigner de vos plus proches amis.", "Eliminer tous les gens pouvant vous empecher d'avancer.",  
    question_6 = "Une attaque survient à Poudlard, que faites-vous ?" #Bonté et Courage
    réponse = "Je vais me cacher dans la tour de ma maison", "Je dégaine ma baguette et fonce au combat", "Je vais aider à soigne les blessés", "Je sors quand je sais que nous sommes entrain de gagner"
    question_7 = "Quelle devise pourrait être la vôtre ?" #Intelligence, Courage, Bonté, Ambition
    réponse = "“L'enseignement devrait être ainsi : celui qui le reçoit le recueille comme un don inestimable mais jamais comme une contrainte pénible.”
              , "Le courage croît en osant et la peur en hésitant.", "Qui est trop bon pour soi ne l'est pas assez pour les autres."
              , "L'homme ambitieux étudie pour connaître, le pédant étudie pour être connu."
    question_8 = 